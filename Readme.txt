
1. Deployed 2 VM's  ( client and server )

2. Clone the specified repository which contains the Todo app 

3. Create Dockerfile under app folder as per instructions

4. Build image using new created Dockerfile  and push the image to Docker hub repo

The image was build using Dockerfile having node:12-apline image as a base image 
Then the new image was pushed to Docker hub repository and it can be pulled using below command 

https://hub.docker.com/repository/docker/alexc24a/alexrepo1

docker pull alexc24a/alexrepo1:latest


5. Create inventory and deployment playbook 

The ansible playbook is performing the below steps on the remote machines :

- Install docker on target machines (specified in the inventory )
- Start diocker service 
- Pull the image from my docker hub repository 
- Add geting-started tag to the image 
- Run the image as a container 


6. Run the ansible playbook to deploy the image on target machine together with all dependencies 


7. Test the application via Google Chrome Browser and add a few items #####